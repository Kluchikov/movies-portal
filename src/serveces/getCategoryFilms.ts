import axios, {AxiosResponse} from "axios";

const getCategoryFilms = (): Promise<AxiosResponse> => axios.get('https://api.tvmaze.com/shows?q=animals')

export default getCategoryFilms