import { AnyAction } from "redux";
import { IStore } from "./types";

const initialState = {
    list: []
}

const CategoryFilmsReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type) {
        case 'CategoryFilms/setCategoryFilms' :
            return {...state, list: [...action.payload]}
        default: 
            return state 
    }
}

export default CategoryFilmsReducer