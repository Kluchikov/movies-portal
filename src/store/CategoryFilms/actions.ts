import { Dispatch } from "redux"
import getCategoryFilms from "../../serveces/getCategoryFilms"
import { IStore } from "./types"

export const setCategoryFilmsAction = (list: IStore['list']) => {
    return{
        type: 'CategoryFilms/setCategoryFilms',
        payload : list,
    }
}

export const loadCategoryFilms = () => async (dispath: Dispatch) => {
    try {
        const response = await getCategoryFilms()

        dispath(setCategoryFilmsAction(response.data))
    } catch (e) {
        console.log(e, 'произошла ошибка')
    }
}