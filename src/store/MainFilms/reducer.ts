import { AnyAction } from "redux";
import { IStore } from "./types";

const initialState = {
    list: []
}

const MainFilmsReducer = (state: IStore = initialState, action: AnyAction) => {
    switch(action.type) {
        case 'MainFilms/setMainFilms' :
            return {...state, list: [...action.mainLoad]}
        default: 
            return state 
    }
}


export default MainFilmsReducer