import { IStore } from "./types"

export const MainSelectList = (state: {MainFilmsReducer: IStore}): IStore['list'] => state.MainFilmsReducer.list 