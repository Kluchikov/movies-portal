import MainImage from '../../images/Emma.png'

import './style.scss'

const InfoPageContent = () => {
    
    return (
        <div className='portal-info'>
            <div>
                <img className='info-page-image' src={MainImage} alt='Emma-Watson'/>
            </div>
            <div  className='portal-info-title'>
                <h2 className='info-page-header'>MOVIES</h2>
                <p className='info-page-text'>Лучший онлайн-кинотеатр в мире. Все новые фильмы тут.</p>
            </div>
        </div>
    )
}

export default InfoPageContent