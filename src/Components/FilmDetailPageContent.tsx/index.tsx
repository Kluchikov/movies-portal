import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { DetailSelectList } from "../../store/DetailFilms/selectors";

import { ID } from "../../types/ID";
import { loadDetail } from "../../store/DetailFilms/actions";

import prepareDate from "../../utils/prepareDate";
import prepareCountry from "../../utils/prepareCountry";
import prepareSummary from "../../utils/prepareSummary";
import prepareRating from "../../utils/prepareRating";
import prepareImage from "../../utils/prepareImage";
import Star from '../../images/star.png'

import './style.scss'

const FilmDetailPageContent = () => {

    const { id } = useParams<ID>()
	const dispDetail = useDispatch()
	const showDetail = useSelector(DetailSelectList)

	useEffect(() => {
		dispDetail(loadDetail(id))
	}, [id, dispDetail])
    console.log(showDetail)

    return (
        <div className="wrapper">
            {showDetail.length > 0 ? (
                <div className="film-detail">
                    {prepareImage(showDetail[0].image?.medium)}
                    <div className="film_info">
                        <div className="detail-head">
                            <div className="film-name">{showDetail[0].name}</div>
                            <div>
                                <img className="star" src={Star} alt="star" />
                                <span>{prepareRating(showDetail[0].rating.average)}/10</span>
                            </div>
                        </div>
                        <div><span className="film-detail-info">Год выхода:</span> <span className="film-detail-info-value">{prepareDate(showDetail[0].premiered)}</span></div>
                        <div><span className="film-detail-info">Страна:</span> <span className="film-detail-info-value">{prepareCountry(showDetail[0].network?.country.name)}</span></div>
                        <div><span className="film-detail-info">Жанр:</span> <span className="film-detail-info-value">{showDetail[0].genres.join(', ')}</span></div>
                        <div><span className="film-detail-info">Описание:</span> <span className="film-detail-info-value">{prepareSummary(showDetail[0].summary)}</span></div>
                    </div>
                </div>
            ) : ('Загрузка')}
        </div>
    )
}

export default FilmDetailPageContent